import $ from 'jquery';
import 'bootstrap';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
import './index.scss';

// Import all images from images folder
function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
}
const images = importAll(require.context('./images', false, /\.(png|jpe?g|svg)$/));

$(document).ready(function () {
    // Animation for hamburger icon
    $('.navbar-toggler').on('click', function () {
      $('.animated-icon').toggleClass('open');
    });


    // Partners Carousel Settings
    $('.owl-carousel').owlCarousel({
      center: true,
      loop:true,
      margin:10,
      nav:false,
      dots:false,
      autoplay: true,
      slideTransition: 'linear',
      autoplayTimeout: 6000,
      autoplaySpeed: 6000,
      autoplayHoverPause: true,
      touchDrag: false,
      mouseDrag: false,
      responsive:{
        0:{
            items:2
        },
        576:{
          items:3
        },
        768:{
          items:4
        },
        992:{
          items:5
        },
        1000:{
            items:6
        }
    }
    });
});
